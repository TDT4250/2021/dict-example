# tdt4250.dict3.no bundle

This bundle is part of variant 3 of the [tdt4250.dict project](../README.md), and was created using the Component template. It provides a **Dict** component that loads a Norwegian dictionary (slightly outdated version of the one used in by Wordfeud) from an included file.

## Packages

- **tdt4250.dict3.no**: The **Dict** implementation.
