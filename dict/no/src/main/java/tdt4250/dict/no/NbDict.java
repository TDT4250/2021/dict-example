package tdt4250.dict.no;

import org.osgi.service.component.annotations.Component;

import tdt4250.dict.api.Dict;
import tdt4250.dict.util.WordsDict;

@Component(
		property = {
				WordsDict.DICT_NAME_PROP + "=nb",
				WordsDict.DICT_RESOURCE_PROP + "=tdt4250.dict.no#/tdt4250/dict/no/nb.txt"}
		)
public class NbDict extends WordsDict implements Dict {
}
