package tdt4250.dict.util;

public interface MutableWords extends Words {
	public void addWord(CharSequence word);
}
