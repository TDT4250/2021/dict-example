package tdt4250.dict.util;

public interface Words extends Iterable<CharSequence> {
	public boolean hasWord(CharSequence word);
}
