# dict-example

Dictionary example (corresponds to dict3 variant from examples repository)

## Build and run

`cd dict` and then build with `mvn clean install`

Check if it resolves for the purpose of running with `mvn bnd-resolver:resolve -pl servlet` and
then create runnable jar with `mvn bnd-export:export -pl servlet`.

Finally, launch with `java -jar servlet/target/launch.jar``.
The service will listen to port 8080, and you can open a browser and use `/dict?q=hei` to lookup `hei`. You should get a match in the `nb` dictionary.

You may also lookup in the gogo shell with `dict:lookup hei`.
